/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 *
 * @author Seba
 */
public class Conexion {
    private static Conexion instance = null;
    private static Connection conn;
    private final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
    private final String username = "system";
    private final String password = "seba";
    private final String DataBaseName = "jdbc:oracle:thin:@localhost:1521:XE";
    
    public Conexion(){}
    
    public static Conexion getInstance(){
        if (instance == null) {
            instance = new Conexion();
        }
        return instance;
    }
    
    public Connection conectar(){
        
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DataBaseName, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ex.getMessage());
        }
        return conn;
    }
   
    
    
    
    
//    public Statement Conectar()
//    {
//     try
//     {
//       Class.forName(JDBC_DRIVER);
//       conn = DriverManager.getConnection(DataBaseName,username,password);
//       return conn.createStatement();
//       
//     }catch(Exception ex)
//     {
//         return null;
//     }
//    }
//    
//    public PreparedStatement Conectar(String query)
//    {
//        try
//        {
//            Class.forName(JDBC_DRIVER);
//            conn = DriverManager.getConnection(DataBaseName,username,password);
//            return conn.prepareStatement(query);
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public void CerrarConexion()
//    {
//        try
//        {
//            conn.close();
//        }catch(Exception e)
//        {
//            
//        }
//    }
    
}

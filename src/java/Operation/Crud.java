/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

import java.util.ArrayList;

/**
 *
 * @author Seba
 * @param <Object>
 */
public interface Crud<Object> {
    
    void agregar(Object dto);   
    Object seleccionarPorId(int id);
    void eliminar(int id);
    void modificar(Object dto);
    ArrayList<Object> listar();  
}

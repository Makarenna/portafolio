/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Seba
 */
public class Servidor {
    private int IDServidor;
    private String Nombre;
    private int Sis_operativo;
    private String Procesador;
    private String Disco_duro;
    private String Mem_ram;
    private String Direc_ip;
    private int Rack;
    private int Garantia;
    private int Tipo;
    private String Contrasenia;
    private int Encargado;
    
    public Servidor(){
        this.IDServidor = 0;
        this.Nombre = "";
        this.Sis_operativo = 0;
        this.Procesador = "";
        this.Disco_duro = "";
        this.Mem_ram = "";
        this.Direc_ip = "";
        this.Rack = 0;
        this.Garantia = 0;
        this.Tipo = 0;
        this.Contrasenia = "";
        this.Encargado = 0;
    }

    public int getIDServidor() {
        return IDServidor;
    }

    public void setIDServidor(int IDServidor) {
        this.IDServidor = IDServidor;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getSis_operativo() {
        return Sis_operativo;
    }

    public void setSis_operativo(int Sis_operativo) {
        this.Sis_operativo = Sis_operativo;
    }

    public String getProcesador() {
        return Procesador;
    }

    public void setProcesador(String Procesador) {
        this.Procesador = Procesador;
    }

    public String getDisco_duro() {
        return Disco_duro;
    }

    public void setDisco_duro(String Disco_duro) {
        this.Disco_duro = Disco_duro;
    }

    public String getMem_ram() {
        return Mem_ram;
    }

    public void setMem_ram(String Mem_ram) {
        this.Mem_ram = Mem_ram;
    }

    public String getDirec_ip() {
        return Direc_ip;
    }

    public void setDirec_ip(String Direc_ip) {
        this.Direc_ip = Direc_ip;
    }

    public int getRack() {
        return Rack;
    }

    public void setRack(int Rack) {
        this.Rack = Rack;
    }

    public int getGarantia() {
        return Garantia;
    }

    public void setGarantia(int Garantia) {
        this.Garantia = Garantia;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int Tipo) {
        this.Tipo = Tipo;
    }

    public String getContrasenia() {
        return Contrasenia;
    }

    public void setContrasenia(String Contrasenia) {
        this.Contrasenia = Contrasenia;
    }

    public int getEncargado() {
        return Encargado;
    }

    public void setEncargado(int Encargado) {
        this.Encargado = Encargado;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Seba
 */
public class SistemaDTO {
    
    private int IDSistema;
    private String Nombre;
    private String Lenguaje_desarrollo;
    private int Encargado;
    private String Descripcion;
    
    public SistemaDTO(){
        this.IDSistema = 0;
        this.Nombre = "";
        this.Lenguaje_desarrollo = "";
        this.Encargado = 0;
        this.Descripcion = "";
    }

    public int getIDSistema() {
        return IDSistema;
    }

    public void setIDSistema(int IDSistema) {
        this.IDSistema = IDSistema;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getLenguaje_desarrollo() {
        return Lenguaje_desarrollo;
    }

    public void setLenguaje_desarrollo(String Lenguaje_desarrollo) {
        this.Lenguaje_desarrollo = Lenguaje_desarrollo;
    }

    public int getEncargado() {
        return Encargado;
    }

    public void setEncargado(int Encargado) {
        this.Encargado = Encargado;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }
}

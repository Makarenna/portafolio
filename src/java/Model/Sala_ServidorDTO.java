/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Seba
 */
public class Sala_ServidorDTO {
    private int IDSala;
    private String Nombre;
    private String Pais;
    private String Region;
    private String Comuna;
    private String Calle;
    private String Tel_contacto;
    private int Encargado;
    
    private int IDU;
    private String NomU;
    
    public Sala_ServidorDTO(){
        this.IDSala = 0;
        this.Nombre = "";
        this.Pais = "";
        this.Region = "";
        this.Comuna = "";
        this.Calle = "";
        this.Tel_contacto = "";
        this.Encargado = 0;
        
    }

    public int getIDSala() {
        return IDSala;
    }

    public void setIDSala(int IDSala) {
        this.IDSala = IDSala;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getPais() {
        return Pais;
    }

    public void setPais(String Pais) {
        this.Pais = Pais;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getComuna() {
        return Comuna;
    }

    public void setComuna(String Comuna) {
        this.Comuna = Comuna;
    }

    public String getCalle() {
        return Calle;
    }

    public void setCalle(String Calle) {
        this.Calle = Calle;
    }

    public String getTel_contacto() {
        return Tel_contacto;
    }

    public void setTel_contacto(String Tel_contacto) {
        this.Tel_contacto = Tel_contacto;
    }

    public int getEncargado() {
        return Encargado;
    }

    public void setEncargado(int Encargado) {
        this.Encargado = Encargado;
    }
}

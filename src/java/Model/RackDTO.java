/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Seba
 */
public class RackDTO {
    
    private int IDRack;
    private int Numero;
    private int Capacidad;
    private int Sala_servidor;
    
    public RackDTO(){
        this.IDRack = 0;
        this.Numero = 0;
        this.Capacidad = 0;
        this.Sala_servidor = 0;
    }

    public int getIDRack() {
        return IDRack;
    }

    public void setIDRack(int IDRack) {
        this.IDRack = IDRack;
    }

    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    public int getCapacidad() {
        return Capacidad;
    }

    public void setCapacidad(int Capacidad) {
        this.Capacidad = Capacidad;
    }

    public int getSala_servidor() {
        return Sala_servidor;
    }

    public void setSala_servidor(int Sala_servidor) {
        this.Sala_servidor = Sala_servidor;
    }
}

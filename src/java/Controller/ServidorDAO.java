/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import Model.ServidorDTO;
import Operation.Crud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class ServidorDAO implements Crud<ServidorDTO> {

    private static Conexion conn;
    
    @Override
    public void agregar(ServidorDTO sr1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "INSERT INTO homeros.Servidor (IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado) VALUES (homeros.autoincrement_Servidor.nextval,?,?,?,?,?,?,?,?,?,?,?) ";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, sr1.getNombre());
            ps.setInt(2, sr1.getSis_operativo());
            ps.setString(3, sr1.getProcesador());
            ps.setString(4, sr1.getDisco_duro());
            ps.setString(5, sr1.getMem_ram());
            ps.setString(6, sr1.getDirec_ip());
            ps.setInt(7, sr1.getRack());
            ps.setInt(8, sr1.getGarantia());
            ps.setInt(9, sr1.getTipo());
            ps.setString(10, sr1.getContrasenia());
            ps.setInt(11, sr1.getEncargado());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }        
    }
    
    public static ServidorDTO SeleccionarPorID(int IDServidor)
    {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado FROM homeros.Servidor WHERE IDServidor = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDServidor);
            
            ResultSet rs = ps.executeQuery();
            
            ServidorDTO sr1 = new ServidorDTO();
            
            while(rs.next())
            {
                sr1.setIDServidor(rs.getInt("IDServidor"));
                sr1.setNombre(rs.getString("Nombre"));
                sr1.setSis_operativo(rs.getInt("Sis_operativo"));
                sr1.setProcesador(rs.getString("Procesador"));
                sr1.setDisco_duro(rs.getString("Disco_duro"));
                sr1.setMem_ram(rs.getString("Mem_ram"));
                sr1.setDirec_ip(rs.getString("Direc_ip"));
                sr1.setRack(rs.getInt("Rack"));
                sr1.setGarantia(rs.getInt("Garantia"));
                sr1.setTipo(rs.getInt("Tipo"));
                sr1.setContrasenia(rs.getString("Contrasenia"));
                sr1.setEncargado(rs.getInt("Encargado"));
            }
            
            return sr1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }    

    @Override
    public ServidorDTO seleccionarPorId(int IDServidor) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado FROM homeros.Servidor WHERE IDServidor = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDServidor);
            
            ResultSet rs = ps.executeQuery();
            
            ServidorDTO sr1 = new ServidorDTO();
            
            while(rs.next())
            {
                sr1.setIDServidor(rs.getInt("IDServidor"));
                sr1.setNombre(rs.getString("Nombre"));
                sr1.setSis_operativo(rs.getInt("Sis_operativo"));
                sr1.setProcesador(rs.getString("Procesador"));
                sr1.setDisco_duro(rs.getString("Disco_duro"));
                sr1.setMem_ram(rs.getString("Mem_ram"));
                sr1.setDirec_ip(rs.getString("Direc_ip"));
                sr1.setRack(rs.getInt("Rack"));
                sr1.setGarantia(rs.getInt("Garantia"));
                sr1.setTipo(rs.getInt("Tipo"));
                sr1.setContrasenia(rs.getString("Contrasenia"));
                sr1.setEncargado(rs.getInt("Encargado"));
            }
            
            return sr1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public void modificar(ServidorDTO sr1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "UPDATE homeros.Servidor SET Nombre = ?, Sis_operativo = ?, Procesador = ?, Disco_duro = ?, Mem_ram = ?, Direc_ip = ?, Rack = ?, Garantia = ?, Tipo = ?, Contrasenia = ?, Encargado = ? WHERE IDServidor = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, sr1.getNombre());
            ps.setInt(2, sr1.getSis_operativo());
            ps.setString(3, sr1.getProcesador());
            ps.setString(4, sr1.getDisco_duro());
            ps.setString(5, sr1.getMem_ram());
            ps.setString(6, sr1.getDirec_ip());
            ps.setInt(7, sr1.getRack());
            ps.setInt(8, sr1.getGarantia());
            ps.setInt(9, sr1.getTipo());
            ps.setString(10, sr1.getContrasenia());
            ps.setInt(11, sr1.getEncargado());
            ps.setInt(12, sr1.getIDServidor());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }        
    }

    @Override
    public ArrayList<ServidorDTO> listar() {
        ArrayList<ServidorDTO> lista = new ArrayList<ServidorDTO>();
        
        try
        {
            conn = Conexion.getInstance();
            
            Statement stm = conn.conectar().createStatement();
            
            String query = "SELECT IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado FROM homeros.Servidor";
            
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next())
            {
                ServidorDTO sr1 = new ServidorDTO();
                
                sr1.setIDServidor(rs.getInt("IDServidor"));
                sr1.setNombre(rs.getString("Nombre"));
                sr1.setSis_operativo(rs.getInt("Sis_operativo"));
                sr1.setProcesador(rs.getString("Procesador"));
                sr1.setDisco_duro(rs.getString("Disco_duro"));
                sr1.setMem_ram(rs.getString("Mem_ram"));
                sr1.setDirec_ip(rs.getString("Direc_ip"));
                sr1.setRack(rs.getInt("Rack"));
                sr1.setGarantia(rs.getInt("Garantia"));
                sr1.setTipo(rs.getInt("Tipo"));
                sr1.setContrasenia(rs.getString("Contrasenia"));
                sr1.setEncargado(rs.getInt("Encargado"));
                
                lista.add(sr1);
            }
            
            return lista;
            
        }catch(Exception ex)
        {
            return null;
        }        
    }
    
//    public static void eliminar(int IDServidor)
//    {
//        try
//        {
//        conn = Conexion.getInstance();
//        
//        String query = "DELETE FROM homeros.Servidor WHERE IDServidor = ?";
//        
//        PreparedStatement ps = conn.conectar().prepareStatement(query);
//        
//        ps.setInt(1, IDServidor);
//        
//        ps.executeUpdate();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }

    @Override
    public void eliminar(int IDServidor) {
                try
        {
        conn = Conexion.getInstance();
        
        String query = "DELETE FROM homeros.Servidor WHERE IDServidor = ?";
        
        PreparedStatement ps = conn.conectar().prepareStatement(query);
        
        ps.setInt(1, IDServidor);
        
        ps.executeUpdate();
        
        }catch(Exception ex)
        {
            
        }
    }
    
}

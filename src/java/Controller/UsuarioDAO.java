/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import Model.UsuarioDTO;
import Operation.Crud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class UsuarioDAO implements Crud<UsuarioDTO> {

    private static Conexion conn;
    
    public boolean autenticacion(String username, String pass){
        try{
            conn = Conexion.getInstance();
            String query = "SELECT * FROM homeros.Usuario WHERE Username = ? AND Pass = ?";
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, username);
            ps.setString(2, pass);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                return true;
            }
            
        }catch(Exception ex){
            
        }
        
        return false;
    }
    
    
    
    @Override
    public void agregar(UsuarioDTO u1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "INSERT INTO homeros.Usuario (Rut,DV,Nombre,Sexo,Edad,Direccion,Correo,Telefono,Rol,Apellidos,Username,Pass) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, u1.getRut());
            ps.setString(2, u1.getDV());
            ps.setString(3, u1.getNombre());
            ps.setString(4, u1.getSexo());
            ps.setInt(5, u1.getEdad());
            ps.setString(6, u1.getDireccion());
            ps.setString(7, u1.getCorreo());
            ps.setString(8, u1.getTelefono());
            ps.setString(9, u1.getRol());
            ps.setString(10, u1.getApellidos());
            ps.setString(11, u1.getUsername());
            ps.setString(12, u1.getPass());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }
    
//        public static UsuarioDTO SeleccionarPorID(int Rut)
//    {
//        try
//        {
//            conn = Conexion.getInstance();
//            
//            String query = "SELECT Rut, DV, Nombre, Sexo, Edad, Direccion, Correo, Telefono, Rol, Apellidos FROM homeros.Usuario WHERE Rut = ?";
//            
//            PreparedStatement ps = conn.conectar().prepareStatement(query);
//            
//            ps.setInt(1, Rut);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            UsuarioDTO u1 = new UsuarioDTO();
//            
//            while(rs.next())
//            {
//                u1.setRut(rs.getInt("Rut"));
//                u1.setDV(rs.getString("DV"));
//                u1.setNombre(rs.getString("Nombre"));
//                u1.setSexo(rs.getString("Sexo"));
//                u1.setEdad(rs.getInt("Edad"));
//                u1.setDireccion(rs.getString("Direccion"));
//                u1.setCorreo(rs.getString("Correo"));
//                u1.setTelefono(rs.getString("Telefono"));
//                u1.setRol(rs.getString("Rol"));
//                u1.setApellidos(rs.getString("Apellidos"));
//            }
//            
//            return u1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }

    @Override
    public UsuarioDTO seleccionarPorId(int rut) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT Rut, DV, Nombre, Sexo, Edad, Direccion, Correo, Telefono, Rol, Apellidos, Username, Pass FROM homeros.Usuario WHERE Rut = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, rut);
            
            ResultSet rs = ps.executeQuery();
            
            UsuarioDTO u1 = new UsuarioDTO();
            
            while(rs.next())
            {
                u1.setRut(rs.getInt("Rut"));
                u1.setDV(rs.getString("DV"));
                u1.setNombre(rs.getString("Nombre"));
                u1.setSexo(rs.getString("Sexo"));
                u1.setEdad(rs.getInt("Edad"));
                u1.setDireccion(rs.getString("Direccion"));
                u1.setCorreo(rs.getString("Correo"));
                u1.setTelefono(rs.getString("Telefono"));
                u1.setRol(rs.getString("Rol"));
                u1.setApellidos(rs.getString("Apellidos"));
                u1.setUsername(rs.getString("Username"));
                u1.setPass(rs.getString("Pass"));
            }
            
            return u1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    
//    public static void eliminar(int rut) {
//        try
//        {
//            conn = Conexion.getInstance();
//
//            String query = "DELETE FROM homeros.Usuario WHERE Rut = ?";
//
//            PreparedStatement ps = conn.conectar().prepareStatement(query);
//
//            ps.setInt(1, rut);
//
//            ps.executeUpdate();
//        }catch(Exception ex)
//        {
//        }
//    }

    @Override
    public void modificar(UsuarioDTO u1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "UPDATE homeros.Usuario SET Nombre = ?, Sexo = ?, Edad = ?, Direccion = ?, Correo = ?, Telefono = ?, Rol = ?, Apellidos = ?, Pass = ? WHERE Rut = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, u1.getNombre());
            ps.setString(2, u1.getSexo());
            ps.setInt(3, u1.getEdad());
            ps.setString(4, u1.getDireccion());
            ps.setString(5, u1.getCorreo());
            ps.setString(6, u1.getTelefono());
            ps.setString(7, u1.getRol());
            ps.setString(8, u1.getApellidos());
            ps.setString(9, u1.getPass());
            ps.setInt(10, u1.getRut());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }

    @Override
    public ArrayList<UsuarioDTO> listar() {
        ArrayList<UsuarioDTO> lista = new ArrayList<UsuarioDTO>();
        try
        {
            conn = Conexion.getInstance();
            
            Statement stm = conn.conectar().createStatement();
            
            String query = "SELECT Rut,DV,Nombre,Sexo,Edad,Direccion,Correo,Telefono,Rol,Apellidos,Username,Pass FROM homeros.Usuario";
            
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next())
            {
                UsuarioDTO u1 = new UsuarioDTO();
                
                u1.setRut(rs.getInt("Rut"));
                u1.setDV(rs.getString("DV"));
                u1.setNombre(rs.getString("Nombre"));
                u1.setSexo(rs.getString("Sexo"));
                u1.setEdad(rs.getInt("Edad"));
                u1.setDireccion(rs.getString("Direccion"));
                u1.setCorreo(rs.getString("Correo"));
                u1.setTelefono(rs.getString("Telefono"));
                u1.setRol(rs.getString("Rol"));
                u1.setApellidos(rs.getString("Apellidos"));
                u1.setUsername(rs.getString("Username"));
                u1.setPass(rs.getString("Pass"));
                  
                lista.add(u1);
            }
          
            
            return lista;
            
        }catch(Exception ex)
        {
            return null;
        }
    }
    
    //private static Conexion conn;
    
    //data access object
    //operaciones a consumo de datos de fuentes externas
    
//    public static void addUser(UsuarioDTO u1){
//        try
//        {
//            conn = Conexion.getInstance();
//            
//            String query = "INSERT INTO homeros.Usuario (Rut,DV,Nombre,Sexo,Edad,Direccion,Correo,Telefono,Rol,Apellidos) VALUES (?,?,?,?,?,?,?,?,?,?) ";
//            
//            PreparedStatement ps = conn.conectar().prepareStatement(query);
//            
//            ps.setInt(1, u1.getRut());
//            ps.setString(2, u1.getDV());
//            ps.setString(3, u1.getNombre());
//            ps.setString(4, u1.getSexo());
//            ps.setInt(5, u1.getEdad());
//            ps.setString(6, u1.getDireccion());
//            ps.setString(7, u1.getCorreo());
//            ps.setString(8, u1.getTelefono());
//            ps.setString(9, u1.getRol());
//            ps.setString(10, u1.getApellidos());
//            
//            ps.executeUpdate();
//            
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//        public static UsuarioDTO SeleccionarPorID(int Rut)
//    {
//        try
//        {
//            conn = Conexion.getInstance();
//            
//            String query = "SELECT Rut, DV, Nombre, Sexo, Edad, Direccion, Correo, Telefono, Rol, Apellidos FROM homeros.Usuario WHERE Rut = ?";
//            
//            PreparedStatement ps = conn.conectar().prepareStatement(query);
//            
//            ps.setInt(1, Rut);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            UsuarioDTO u1 = new UsuarioDTO();
//            
//            while(rs.next())
//            {
//                u1.setRut(rs.getInt("Rut"));
//                u1.setDV(rs.getString("DV"));
//                u1.setNombre(rs.getString("Nombre"));
//                u1.setSexo(rs.getString("Sexo"));
//                u1.setEdad(rs.getInt("Edad"));
//                u1.setDireccion(rs.getString("Direccion"));
//                u1.setCorreo(rs.getString("Correo"));
//                u1.setTelefono(rs.getString("Telefono"));
//                u1.setRol(rs.getString("Rol"));
//                u1.setApellidos(rs.getString("Apellidos"));
//            }
//            
//            return u1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//        
//   public static ArrayList<UsuarioDTO> Listar()
//    {
//        ArrayList<UsuarioDTO> lista = new ArrayList<UsuarioDTO>();
//        
//        try
//        {
//            conn = Conexion.getInstance();
//            
//            Statement stm = conn.conectar().createStatement();
//            
//            String query = "SELECT Rut,DV,Nombre,Sexo,Edad,Direccion,Correo,Telefono,Rol,Apellidos FROM homeros.Usuario";
//            
//            ResultSet rs = stm.executeQuery(query);
//            
//            while(rs.next())
//            {
//                UsuarioDTO u1 = new UsuarioDTO();
//                
//                u1.setRut(rs.getInt("Rut"));
//                u1.setDV(rs.getString("DV"));
//                u1.setNombre(rs.getString("Nombre"));
//                u1.setSexo(rs.getString("Sexo"));
//                u1.setEdad(rs.getInt("Edad"));
//                u1.setDireccion(rs.getString("Direccion"));
//                u1.setCorreo(rs.getString("Correo"));
//                u1.setTelefono(rs.getString("Telefono"));
//                u1.setRol(rs.getString("Rol"));
//                u1.setApellidos(rs.getString("Apellidos"));
//                
//                lista.add(u1);
//            }
//          
//            
//            return lista;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }

    @Override
    public void eliminar(int rut) {
        try
        {
            conn = Conexion.getInstance();

            String query = "DELETE FROM homeros.Usuario WHERE Rut = ?";

            PreparedStatement ps = conn.conectar().prepareStatement(query);

            ps.setInt(1, rut);

            ps.executeUpdate();
        }catch(Exception ex)
        {
            
        }
    }
    
}

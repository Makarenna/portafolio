/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import Model.RackDTO;
import Operation.Crud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class RackDAO implements Crud<RackDTO> {

    private static Conexion conn;
    
    @Override
    public void agregar(RackDTO r1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "INSERT INTO homeros.Rack (IDRack,Numero,Capacidad,Sala_servidor) VALUES (homeros.autoincrement_Rack.nextval,?,?,?) ";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, r1.getNumero());
            ps.setInt(2, r1.getCapacidad());
            ps.setInt(3, r1.getSala_servidor());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }
    
    public static RackDTO SeleccionarPorID(int IDRack)
    {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDRack,Numero,Capacidad,Sala_servidor FROM homeros.Rack WHERE IDRack = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDRack);
            
            ResultSet rs = ps.executeQuery();
            
            RackDTO r1 = new RackDTO();
            
            while(rs.next())
            {
                r1.setIDRack(rs.getInt("IDRack"));
                r1.setNumero(rs.getInt("Numero"));
                r1.setCapacidad(rs.getInt("Capacidad"));
                r1.setSala_servidor(rs.getInt("Sala_servidor"));
            }
            
            return r1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public RackDTO seleccionarPorId(int IDRack) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDRack,Numero,Capacidad,Sala_servidor FROM homeros.Rack WHERE IDRack = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDRack);
            
            ResultSet rs = ps.executeQuery();
            
            RackDTO r1 = new RackDTO();
            
            while(rs.next())
            {
                r1.setIDRack(rs.getInt("IDRack"));
                r1.setNumero(rs.getInt("Numero"));
                r1.setCapacidad(rs.getInt("Capacidad"));
                r1.setSala_servidor(rs.getInt("Sala_servidor"));
            }
            
            return r1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public void modificar(RackDTO r1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "UPDATE homeros.Rack SET Numero = ?, Capacidad = ?, Sala_servidor = ? WHERE IDRack = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, r1.getNumero());
            ps.setInt(2, r1.getCapacidad());
            ps.setInt(3, r1.getSala_servidor());
            ps.setInt(4, r1.getIDRack());
            

            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }

    @Override
    public ArrayList<RackDTO> listar() {
        ArrayList<RackDTO> lista = new ArrayList<RackDTO>();
        
        try
        {
            conn = Conexion.getInstance();
            
            Statement stm = conn.conectar().createStatement();
            
            String query = "SELECT IDRack,Numero,Capacidad,Sala_servidor FROM homeros.Rack";
            
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next())
            {
                RackDTO r1 = new RackDTO();
                
                r1.setIDRack(rs.getInt("IDRack"));
                r1.setNumero(rs.getInt("Numero"));
                r1.setCapacidad(rs.getInt("Capacidad"));
                r1.setSala_servidor(rs.getInt("Sala_servidor"));
                
                lista.add(r1);
            }
            
            return lista;
            
        }catch(Exception ex)
        {
            return null;
        }
    }
    
    public static void Eliminar(int IDRack)
    {
        try
        {
        conn = Conexion.getInstance();
        
        String query = "DELETE FROM homeros.Rack WHERE IDRack = ?";
        
        PreparedStatement ps = conn.conectar().prepareStatement(query);
        
        ps.setInt(1, IDRack);
        
        ps.executeUpdate();
        
        }catch(Exception ex)
        {
            
        }
    }

    @Override
    public void eliminar(int IDRack) {
        try
        {
        conn = Conexion.getInstance();
        
        String query = "DELETE FROM homeros.Rack WHERE IDRack = ?";
        
        PreparedStatement ps = conn.conectar().prepareStatement(query);
        
        ps.setInt(1, IDRack);
        
        ps.executeUpdate();
        
        }catch(Exception ex)
        {
            
        }
    }
    
}

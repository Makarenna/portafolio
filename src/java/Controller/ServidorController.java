///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Controller;
//
//import Conexion.Conexion;
//import Model.Servidor;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//
///**
// *
// * @author Seba
// */
//public class ServidorController {
//    public static void Agregar(Servidor sr1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "INSERT INTO homeros.Servidor (IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado) VALUES (homeros.autoincrement_Servidor.nextval,?,?,?,?,?,?,?,?,?,?,?) ";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, sr1.getNombre());
//            ps.setInt(2, sr1.getSis_operativo());
//            ps.setString(3, sr1.getProcesador());
//            ps.setString(4, sr1.getDisco_duro());
//            ps.setString(5, sr1.getMem_ram());
//            ps.setString(6, sr1.getDirec_ip());
//            ps.setInt(7, sr1.getRack());
//            ps.setInt(8, sr1.getGarantia());
//            ps.setInt(9, sr1.getTipo());
//            ps.setString(10, sr1.getContrasenia());
//            ps.setInt(11, sr1.getEncargado());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//            
//            
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static ArrayList<Servidor> Listar()
//    {
//        ArrayList<Servidor> lista = new ArrayList<Servidor>();
//        
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            Statement stm = conn.Conectar();
//            
//            String query = "SELECT IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado FROM homeros.Servidor";
//            
//            ResultSet rs = stm.executeQuery(query);
//            
//            while(rs.next())
//            {
//                Servidor sr1 = new Servidor();
//                
//                sr1.setIDServidor(rs.getInt("IDServidor"));
//                sr1.setNombre(rs.getString("Nombre"));
//                sr1.setSis_operativo(rs.getInt("Sis_operativo"));
//                sr1.setProcesador(rs.getString("Procesador"));
//                sr1.setDisco_duro(rs.getString("Disco_duro"));
//                sr1.setMem_ram(rs.getString("Mem_ram"));
//                sr1.setDirec_ip(rs.getString("Direc_ip"));
//                sr1.setRack(rs.getInt("Rack"));
//                sr1.setGarantia(rs.getInt("Garantia"));
//                sr1.setTipo(rs.getInt("Tipo"));
//                sr1.setContrasenia(rs.getString("Contrasenia"));
//                sr1.setEncargado(rs.getInt("Encargado"));
//                
//                lista.add(sr1);
//            }
//            
//            conn.CerrarConexion();
//            
//            return lista;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Modificar(Servidor sr1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "UPDATE homeros.Servidor SET Nombre = ?, Sis_operativo = ?, Procesador = ?, Disco_duro = ?, Mem_ram = ?, Direc_ip = ?, Rack = ?, Garantia = ?, Tipo = ?, Contrasenia = ?, Encargado = ? WHERE IDServidor = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, sr1.getNombre());
//            ps.setInt(2, sr1.getSis_operativo());
//            ps.setString(3, sr1.getProcesador());
//            ps.setString(4, sr1.getDisco_duro());
//            ps.setString(5, sr1.getMem_ram());
//            ps.setString(6, sr1.getDirec_ip());
//            ps.setInt(7, sr1.getRack());
//            ps.setInt(8, sr1.getGarantia());
//            ps.setInt(9, sr1.getTipo());
//            ps.setString(10, sr1.getContrasenia());
//            ps.setInt(11, sr1.getEncargado());
//            ps.setInt(12, sr1.getIDServidor());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static Servidor SeleccionarPorID(int IDServidor)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "SELECT IDServidor,Nombre,Sis_operativo,Procesador,Disco_duro,Mem_ram,Direc_ip,Rack,Garantia,Tipo,Contrasenia,Encargado FROM homeros.Servidor WHERE IDServidor = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, IDServidor);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            Servidor sr1 = new Servidor();
//            
//            while(rs.next())
//            {
//                sr1.setIDServidor(rs.getInt("IDServidor"));
//                sr1.setNombre(rs.getString("Nombre"));
//                sr1.setSis_operativo(rs.getInt("Sis_operativo"));
//                sr1.setProcesador(rs.getString("Procesador"));
//                sr1.setDisco_duro(rs.getString("Disco_duro"));
//                sr1.setMem_ram(rs.getString("Mem_ram"));
//                sr1.setDirec_ip(rs.getString("Direc_ip"));
//                sr1.setRack(rs.getInt("Rack"));
//                sr1.setGarantia(rs.getInt("Garantia"));
//                sr1.setTipo(rs.getInt("Tipo"));
//                sr1.setContrasenia(rs.getString("Contrasenia"));
//                sr1.setEncargado(rs.getInt("Encargado"));
//            }
//            
//            conn.CerrarConexion();
//            
//            return sr1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Eliminar(int IDServidor)
//    {
//        try
//        {
//        Conexion conn = new Conexion();
//        
//        String query = "DELETE FROM homeros.Servidor WHERE IDServidor = ?";
//        
//        PreparedStatement ps = conn.Conectar(query);
//        
//        ps.setInt(1, IDServidor);
//        
//        ps.executeUpdate();
//        
//        conn.CerrarConexion();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//}

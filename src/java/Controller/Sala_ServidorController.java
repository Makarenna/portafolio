///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Controller;
//
//import Conexion.Conexion;
//import Model.Sala_Servidor;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//
///**
// *s
// * @author Seba
// */
//public class Sala_ServidorController {
//    public static void Agregar(Sala_Servidor ss1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "INSERT INTO homeros.Sala_servidor (IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado) VALUES (homeros.auto_increment_Sala.nextval,?,?,?,?,?,?,?) ";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, ss1.getNombre());
//            ps.setString(2, ss1.getPais());
//            ps.setString(3, ss1.getRegion());
//            ps.setString(4, ss1.getComuna());
//            ps.setString(5, ss1.getCalle());
//            ps.setString(6, ss1.getTel_contacto());
//            ps.setInt(7, ss1.getEncargado());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//            
//            
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static ArrayList<Sala_Servidor> Listar()
//    {
//        ArrayList<Sala_Servidor> lista = new ArrayList<Sala_Servidor>();
//        
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            Statement stm = conn.Conectar();
//            
//            String query = "SELECT IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado FROM homeros.Sala_servidor";
//            
//            ResultSet rs = stm.executeQuery(query);
//            
//            while(rs.next())
//            {
//                Sala_Servidor ss1 = new Sala_Servidor();
//                
//                ss1.setIDSala(rs.getInt("IDSala"));
//                ss1.setNombre(rs.getString("Nombre"));
//                ss1.setPais(rs.getString("Pais"));
//                ss1.setRegion(rs.getString("Region"));
//                ss1.setComuna(rs.getString("Comuna"));
//                ss1.setCalle(rs.getString("Calle"));
//                ss1.setTel_contacto(rs.getString("Tel_contacto"));
//                ss1.setEncargado(rs.getInt("Encargado"));
//                
//                lista.add(ss1);
//            }
//            
//            conn.CerrarConexion();
//            
//            return lista;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Modificar(Sala_Servidor ss1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "UPDATE homeros.Sala_servidor SET Nombre = ?, Pais = ?, Region = ?, Comuna = ?, Calle = ?, Tel_contacto = ?, Encargado = ? WHERE IDSala = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, ss1.getNombre());
//            ps.setString(2, ss1.getPais());
//            ps.setString(3, ss1.getRegion());
//            ps.setString(4, ss1.getComuna());
//            ps.setString(5, ss1.getCalle());
//            ps.setString(6, ss1.getTel_contacto());
//            ps.setInt(7, ss1.getEncargado());
//            ps.setInt(8, ss1.getIDSala());
//            
//
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static Sala_Servidor SeleccionarPorID(int IDSala)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "SELECT IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado FROM homeros.Sala_servidor WHERE IDSala = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, IDSala);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            Sala_Servidor ss1 = new Sala_Servidor();
//            
//            while(rs.next())
//            {
//                ss1.setIDSala(rs.getInt("IDSala"));
//                ss1.setNombre(rs.getString("Nombre"));
//                ss1.setPais(rs.getString("Pais"));
//                ss1.setRegion(rs.getString("Region"));
//                ss1.setComuna(rs.getString("Comuna"));
//                ss1.setCalle(rs.getString("Calle"));
//                ss1.setTel_contacto(rs.getString("Tel_contacto"));
//                ss1.setEncargado(rs.getInt("Encargado"));
//            }
//            
//            conn.CerrarConexion();
//            
//            return ss1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Eliminar(int IDSala)
//    {
//        try
//        {
//        Conexion conn = new Conexion();
//        
//        String query = "DELETE FROM homeros.Sala_servidor WHERE IDSala = ?";
//        
//        PreparedStatement ps = conn.Conectar(query);
//        
//        ps.setInt(1, IDSala);
//        
//        ps.executeUpdate();
//        
//        conn.CerrarConexion();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//}

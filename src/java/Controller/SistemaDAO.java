/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import Model.SistemaDTO;
import Operation.Crud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class SistemaDAO implements Crud<SistemaDTO>{
    
    private static Conexion conn;

    @Override
    public void agregar(SistemaDTO s1) {
                try
        {
            conn = Conexion.getInstance();
            
            String query = "INSERT INTO homeros.Sistema (IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion) VALUES (homeros.autoincrementable.nextval,?,?,?,?) ";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, s1.getNombre());
            ps.setString(2, s1.getLenguaje_desarrollo());
            ps.setInt(3, s1.getEncargado());
            ps.setString(4, s1.getDescripcion());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }
    
    public static SistemaDTO SeleccionarPorID(int IDSistema)
    {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion FROM homeros.Sistema WHERE IDSistema = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDSistema);
            
            ResultSet rs = ps.executeQuery();
            
            SistemaDTO s1 = new SistemaDTO();
            
            while(rs.next())
            {
                s1.setIDSistema(rs.getInt("IDSistema"));
                s1.setNombre(rs.getString("Nombre"));
                s1.setLenguaje_desarrollo(rs.getString("Lenguaje_desarrollo"));
                s1.setEncargado(rs.getInt("Encargado"));
                s1.setDescripcion(rs.getString("Descripcion"));
            }
            
            return s1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public SistemaDTO seleccionarPorId(int IDSistema){
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion FROM homeros.Sistema WHERE IDSistema = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDSistema);
            
            ResultSet rs = ps.executeQuery();
            
            SistemaDTO s1 = new SistemaDTO();
            
            while(rs.next())
            {
                s1.setIDSistema(rs.getInt("IDSistema"));
                s1.setNombre(rs.getString("Nombre"));
                s1.setLenguaje_desarrollo(rs.getString("Lenguaje_desarrollo"));
                s1.setEncargado(rs.getInt("Encargado"));
                s1.setDescripcion(rs.getString("Descripcion"));
            }
            
            return s1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public void modificar(SistemaDTO s1) {
                try
        {
            conn = Conexion.getInstance();
            
            String query = "UPDATE homeros.Sistema SET Nombre = ?, Lenguaje_desarrollo = ?, Encargado = ?, Descripcion = ? WHERE IDSistema = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, s1.getNombre());
            ps.setString(2, s1.getLenguaje_desarrollo());
            ps.setInt(3, s1.getEncargado());
            ps.setString(4, s1.getDescripcion());
            ps.setInt(5, s1.getIDSistema());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }

    @Override
    public ArrayList<SistemaDTO> listar() {
        ArrayList<SistemaDTO> lista = new ArrayList<SistemaDTO>();
        try
        {
            conn = Conexion.getInstance();
            
            Statement stm = conn.conectar().createStatement();
            
            String query = "SELECT IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion FROM homeros.Sistema";
            
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next())
            {
                SistemaDTO s1 = new SistemaDTO();
                
                s1.setIDSistema(rs.getInt("IDSistema"));
                s1.setNombre(rs.getString("Nombre"));
                s1.setLenguaje_desarrollo(rs.getString("Lenguaje_desarrollo"));
                s1.setEncargado(rs.getInt("Encargado"));
                s1.setDescripcion(rs.getString("Descripcion"));
                
                lista.add(s1);
            }
            
            
            return lista;
            
        }catch(Exception ex)
        {
            return null;
        }
    }
    
//    public static void eliminar(int IDSistema) {
//        try
//        {
//            conn = Conexion.getInstance();
//
//            String query = "DELETE FROM homeros.Sistema WHERE IDSistema = ?";
//
//            PreparedStatement ps = conn.conectar().prepareStatement(query);
//
//            ps.setInt(1, IDSistema);
//
//            ps.executeUpdate();
//        }catch(Exception ex)
//        {
//            
//        }
//    }

    @Override
    public void eliminar(int IDSistema) {
                try
        {
            conn = Conexion.getInstance();

            String query = "DELETE FROM homeros.Sistema WHERE IDSistema = ?";

            PreparedStatement ps = conn.conectar().prepareStatement(query);

            ps.setInt(1, IDSistema);

            ps.executeUpdate();
        }catch(Exception ex)
        {
            
        }
    }
    
}

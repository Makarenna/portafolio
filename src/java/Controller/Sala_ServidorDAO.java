/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import Model.Sala_ServidorDTO;
import Operation.Crud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class Sala_ServidorDAO implements Crud<Sala_ServidorDTO> {

    private static Conexion conn;
    
    @Override
    public void agregar(Sala_ServidorDTO ss1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "INSERT INTO homeros.Sala_servidor (IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado) VALUES (homeros.auto_increment_Sala.nextval,?,?,?,?,?,?,?) ";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, ss1.getNombre());
            ps.setString(2, ss1.getPais());
            ps.setString(3, ss1.getRegion());
            ps.setString(4, ss1.getComuna());
            ps.setString(5, ss1.getCalle());
            ps.setString(6, ss1.getTel_contacto());
            ps.setInt(7, ss1.getEncargado());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }
    
        public static Sala_ServidorDTO SeleccionarPorID(int IDSala)
    {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado FROM homeros.Sala_servidor WHERE IDSala = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDSala);
            
            ResultSet rs = ps.executeQuery();
            
            Sala_ServidorDTO ss1 = new Sala_ServidorDTO();
            
            while(rs.next())
            {
                ss1.setIDSala(rs.getInt("IDSala"));
                ss1.setNombre(rs.getString("Nombre"));
                ss1.setPais(rs.getString("Pais"));
                ss1.setRegion(rs.getString("Region"));
                ss1.setComuna(rs.getString("Comuna"));
                ss1.setCalle(rs.getString("Calle"));
                ss1.setTel_contacto(rs.getString("Tel_contacto"));
                ss1.setEncargado(rs.getInt("Encargado"));
            }
            
            return ss1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public Sala_ServidorDTO seleccionarPorId(int IDSala) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado FROM homeros.Sala_servidor WHERE IDSala = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDSala);
            
            ResultSet rs = ps.executeQuery();
            
            Sala_ServidorDTO ss1 = new Sala_ServidorDTO();
            
            while(rs.next())
            {
                ss1.setIDSala(rs.getInt("IDSala"));
                ss1.setNombre(rs.getString("Nombre"));
                ss1.setPais(rs.getString("Pais"));
                ss1.setRegion(rs.getString("Region"));
                ss1.setComuna(rs.getString("Comuna"));
                ss1.setCalle(rs.getString("Calle"));
                ss1.setTel_contacto(rs.getString("Tel_contacto"));
                ss1.setEncargado(rs.getInt("Encargado"));
            }
            
            return ss1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public void modificar(Sala_ServidorDTO ss1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "UPDATE homeros.Sala_servidor SET Nombre = ?, Pais = ?, Region = ?, Comuna = ?, Calle = ?, Tel_contacto = ?, Encargado = ? WHERE IDSala = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, ss1.getNombre());
            ps.setString(2, ss1.getPais());
            ps.setString(3, ss1.getRegion());
            ps.setString(4, ss1.getComuna());
            ps.setString(5, ss1.getCalle());
            ps.setString(6, ss1.getTel_contacto());
            ps.setInt(7, ss1.getEncargado());
            ps.setInt(8, ss1.getIDSala());
            

            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }

    @Override
    public ArrayList<Sala_ServidorDTO> listar() {
        ArrayList<Sala_ServidorDTO> lista = new ArrayList<Sala_ServidorDTO>();
        try
        {
            conn = Conexion.getInstance();
            
            Statement stm = conn.conectar().createStatement();
            
            String query = "SELECT IDSala,Nombre,Pais,Region,Comuna,Calle,Tel_contacto,Encargado FROM homeros.Sala_servidor";
            
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next())
            {
                Sala_ServidorDTO ss1 = new Sala_ServidorDTO();
                
                ss1.setIDSala(rs.getInt("IDSala"));
                ss1.setNombre(rs.getString("Nombre"));
                ss1.setPais(rs.getString("Pais"));
                ss1.setRegion(rs.getString("Region"));
                ss1.setComuna(rs.getString("Comuna"));
                ss1.setCalle(rs.getString("Calle"));
                ss1.setTel_contacto(rs.getString("Tel_contacto"));
                ss1.setEncargado(rs.getInt("Encargado"));
                
                lista.add(ss1);
            }
            
            return lista;
            
        }catch(Exception ex)
        {
            return null;
        }
    }
    
//        public static void eliminar(int IDSala)
//    {
//        try
//        {
//        conn = Conexion.getInstance();
//        
//        String query = "DELETE FROM homeros.Sala_servidor WHERE IDSala = ?";
//        
//        PreparedStatement ps = conn.conectar().prepareStatement(query);
//        
//        ps.setInt(1, IDSala);
//        
//        ps.executeUpdate();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }

    @Override
    public void eliminar(int IDSala) {
                try
        {
        conn = Conexion.getInstance();
        
        String query = "DELETE FROM homeros.Sala_servidor WHERE IDSala = ?";
        
        PreparedStatement ps = conn.conectar().prepareStatement(query);
        
        ps.setInt(1, IDSala);
        
        ps.executeUpdate();
        
        }catch(Exception ex)
        {
            
        }
    }
}

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Controller;
//
//import Conexion.Conexion;
//import Model.Rack;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//
///**
// *
// * @author Seba
// */
//public class RackController {
//    public static void Agregar(Rack r1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "INSERT INTO homeros.Rack (IDRack,Numero,Capacidad,Sala_servidor) VALUES (homeros.autoincrement_Rack.nextval,?,?,?) ";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, r1.getNumero());
//            ps.setInt(2, r1.getCapacidad());
//            ps.setInt(3, r1.getSala_servidor());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//            
//            
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static ArrayList<Rack> Listar()
//    {
//        ArrayList<Rack> lista = new ArrayList<Rack>();
//        
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            Statement stm = conn.Conectar();
//            
//            String query = "SELECT IDRack,Numero,Capacidad,Sala_servidor FROM homeros.Rack";
//            
//            ResultSet rs = stm.executeQuery(query);
//            
//            while(rs.next())
//            {
//                Rack r1 = new Rack();
//                
//                r1.setIDRack(rs.getInt("IDRack"));
//                r1.setNumero(rs.getInt("Numero"));
//                r1.setCapacidad(rs.getInt("Capacidad"));
//                r1.setSala_servidor(rs.getInt("Sala_servidor"));
//                
//                lista.add(r1);
//            }
//            
//            conn.CerrarConexion();
//            
//            return lista;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Modificar(Rack r1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "UPDATE homeros.Rack SET Numero = ?, Capacidad = ?, Sala_servidor = ? WHERE IDRack = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, r1.getNumero());
//            ps.setInt(2, r1.getCapacidad());
//            ps.setInt(3, r1.getSala_servidor());
//            ps.setInt(4, r1.getIDRack());
//            
//
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static Rack SeleccionarPorID(int IDRack)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "SELECT IDRack,Numero,Capacidad,Sala_servidor FROM homeros.Rack WHERE IDRack = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, IDRack);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            Rack r1 = new Rack();
//            
//            while(rs.next())
//            {
//                r1.setIDRack(rs.getInt("IDRack"));
//                r1.setNumero(rs.getInt("Numero"));
//                r1.setCapacidad(rs.getInt("Capacidad"));
//                r1.setSala_servidor(rs.getInt("Sala_servidor"));
//            }
//            
//            conn.CerrarConexion();
//            
//            return r1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Eliminar(int IDRack)
//    {
//        try
//        {
//        Conexion conn = new Conexion();
//        
//        String query = "DELETE FROM homeros.Rack WHERE IDRack = ?";
//        
//        PreparedStatement ps = conn.Conectar(query);
//        
//        ps.setInt(1, IDRack);
//        
//        ps.executeUpdate();
//        
//        conn.CerrarConexion();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//}

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Controller;
//
//import Conexion.Conexion;
//import Model.Servicio;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//
///**
// *
// * @author Seba
// */
//public class ServicioController {
//    public static void Agregar(Servicio se1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "INSERT INTO homeros.Servicio (IDServicio,Nombre,Sistema) VALUES (homeros.autoincrement_Servicio.nextval,?,?) ";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, se1.getNombre());
//            ps.setString(2, se1.getSistema());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//            
//            
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static ArrayList<Servicio> Listar()
//    {
//        ArrayList<Servicio> lista = new ArrayList<Servicio>();
//        
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            Statement stm = conn.Conectar();
//            
//            String query = "SELECT IDServicio,Nombre,Sistema FROM homeros.Servicio";
//            
//            ResultSet rs = stm.executeQuery(query);
//            
//            while(rs.next())
//            {
//                Servicio se1 = new Servicio();
//                
//                se1.setIDServicio(rs.getInt("IDServicio"));
//                se1.setNombre(rs.getString("Nombre"));
//                se1.setSistema(rs.getString("Sistema"));
//                
//                lista.add(se1);
//            }
//            
//            conn.CerrarConexion();
//            
//            return lista;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Modificar(Servicio se1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "UPDATE homeros.Servicio SET Nombre = ?, Sistema = ? WHERE IDServicio = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, se1.getNombre());
//            ps.setString(2, se1.getSistema());
//            ps.setInt(3, se1.getIDServicio());           
//
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static Servicio SeleccionarPorID(int IDServicio)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "SELECT IDServicio,Nombre,Sistema FROM homeros.Servicio WHERE IDServicio = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, IDServicio);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            Servicio se1 = new Servicio();
//            
//            while(rs.next())
//            {
//                se1.setIDServicio(rs.getInt("IDServicio"));
//                se1.setNombre(rs.getString("Nombre"));
//                se1.setSistema(rs.getString("Sistema"));
//            }
//            
//            conn.CerrarConexion();
//            
//            return se1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Eliminar(int IDServicio)
//    {
//        try
//        {
//        Conexion conn = new Conexion();
//        
//        String query = "DELETE FROM homeros.Servicio WHERE IDServicio = ?";
//        
//        PreparedStatement ps = conn.Conectar(query);
//        
//        ps.setInt(1, IDServicio);
//        
//        ps.executeUpdate();
//        
//        conn.CerrarConexion();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//}

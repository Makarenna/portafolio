/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Conexion.Conexion;
import Model.ServicioDTO;
import Operation.Crud;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Seba
 */
public class ServicioDAO implements Crud<ServicioDTO> {

    private static Conexion conn;
    
    @Override
    public void agregar(ServicioDTO se1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "INSERT INTO homeros.Servicio (IDServicio,Nombre,Sistema) VALUES (homeros.autoincrement_Servicio.nextval,?,?) ";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, se1.getNombre());
            ps.setString(2, se1.getSistema());
            
            ps.executeUpdate();
            
        }catch(Exception ex)
        {
            
        }
    }
    
    public static ServicioDTO SeleccionarPorID(int IDServicio)
    {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDServicio,Nombre,Sistema FROM homeros.Servicio WHERE IDServicio = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDServicio);
            
            ResultSet rs = ps.executeQuery();
            
            ServicioDTO se1 = new ServicioDTO();
            
            while(rs.next())
            {
                se1.setIDServicio(rs.getInt("IDServicio"));
                se1.setNombre(rs.getString("Nombre"));
                se1.setSistema(rs.getString("Sistema"));
            }
            
            return se1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public ServicioDTO seleccionarPorId(int IDServicio) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "SELECT IDServicio,Nombre,Sistema FROM homeros.Servicio WHERE IDServicio = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setInt(1, IDServicio);
            
            ResultSet rs = ps.executeQuery();
            
            ServicioDTO se1 = new ServicioDTO();
            
            while(rs.next())
            {
                se1.setIDServicio(rs.getInt("IDServicio"));
                se1.setNombre(rs.getString("Nombre"));
                se1.setSistema(rs.getString("Sistema"));
            }
            
            return se1;
            
        }catch(Exception ex)
        {
            return null;
        }
    }

    @Override
    public void modificar(ServicioDTO se1) {
        try
        {
            conn = Conexion.getInstance();
            
            String query = "UPDATE homeros.Servicio SET Nombre = ?, Sistema = ? WHERE IDServicio = ?";
            
            PreparedStatement ps = conn.conectar().prepareStatement(query);
            
            ps.setString(1, se1.getNombre());
            ps.setString(2, se1.getSistema());
            ps.setInt(3, se1.getIDServicio());
            
            ps.executeUpdate();
        }catch(Exception ex)
        {
            
        }        
    }

    @Override
    public ArrayList<ServicioDTO> listar() {
        ArrayList<ServicioDTO> lista = new ArrayList<ServicioDTO>();
        
        try
        {
            conn = Conexion.getInstance();
            
            Statement stm = conn.conectar().createStatement();
            
            String query = "SELECT IDServicio,Nombre,Sistema FROM homeros.Servicio";
            
            ResultSet rs = stm.executeQuery(query);
            
            while(rs.next())
            {
                ServicioDTO se1 = new ServicioDTO();
                
                se1.setIDServicio(rs.getInt("IDServicio"));
                se1.setNombre(rs.getString("Nombre"));
                se1.setSistema(rs.getString("Sistema"));
                
                lista.add(se1);
            }
            
            return lista;
            
        }catch(Exception ex)
        {
            return null;
        }
    }
    
//    public static void eliminar(int IDServicio)
//    {
//        try
//        {
//        conn = Conexion.getInstance();
//        
//        String query = "DELETE FROM homeros.Servicio WHERE IDServicio = ?";
//        
//        PreparedStatement ps = conn.conectar().prepareStatement(query);
//        
//        ps.setInt(1, IDServicio);
//        
//        ps.executeUpdate();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }

    @Override
    public void eliminar(int IDServicio) {
                try
        {
        conn = Conexion.getInstance();
        
        String query = "DELETE FROM homeros.Servicio WHERE IDServicio = ?";
        
        PreparedStatement ps = conn.conectar().prepareStatement(query);
        
        ps.setInt(1, IDServicio);
        
        ps.executeUpdate();
        
        }catch(Exception ex)
        {
            
        }
    }
    
}

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Controller;
//
//import Conexion.Conexion;
//import Model.Sistema;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//
///**
// *
// * @author Seba
// */
//public class SistemaController {
//    public static void Agregar(Sistema s1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "INSERT INTO homeros.Sistema (IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion) VALUES (homeros.autoincrementable.nextval,?,?,?,?) ";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, s1.getNombre());
//            ps.setString(2, s1.getLenguaje_desarrollo());
//            ps.setInt(3, s1.getEncargado());
//            ps.setString(4, s1.getDescripcion());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//            
//            
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static ArrayList<Sistema> Listar()
//    {
//        ArrayList<Sistema> lista = new ArrayList<Sistema>();
//        
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            Statement stm = conn.Conectar();
//            
//            String query = "SELECT IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion FROM homeros.Sistema";
//            
//            ResultSet rs = stm.executeQuery(query);
//            
//            while(rs.next())
//            {
//                Sistema s1 = new Sistema();
//                
//                s1.setIDSistema(rs.getInt("IDSistema"));
//                s1.setNombre(rs.getString("Nombre"));
//                s1.setLenguaje_desarrollo(rs.getString("Lenguaje_desarrollo"));
//                s1.setEncargado(rs.getInt("Encargado"));
//                s1.setDescripcion(rs.getString("Descripcion"));
//                
//                lista.add(s1);
//            }
//            
//            conn.CerrarConexion();
//            
//            return lista;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Modificar(Sistema s1)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "UPDATE homeros.Sistema SET Nombre = ?, Lenguaje_desarrollo = ?, Encargado = ?, Descripcion = ? WHERE IDSistema = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setString(1, s1.getNombre());
//            ps.setString(2, s1.getLenguaje_desarrollo());
//            ps.setInt(3, s1.getEncargado());
//            ps.setString(4, s1.getDescripcion());
//            ps.setInt(5, s1.getIDSistema());
//            
//            ps.executeUpdate();
//            
//            conn.CerrarConexion();
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//    
//    public static Sistema SeleccionarPorID(int IDSistema)
//    {
//        try
//        {
//            Conexion conn = new Conexion();
//            
//            String query = "SELECT IDSistema,Nombre,Lenguaje_desarrollo,Encargado,Descripcion FROM homeros.Sistema WHERE IDSistema = ?";
//            
//            PreparedStatement ps = conn.Conectar(query);
//            
//            ps.setInt(1, IDSistema);
//            
//            ResultSet rs = ps.executeQuery();
//            
//            Sistema s1 = new Sistema();
//            
//            while(rs.next())
//            {
//                s1.setIDSistema(rs.getInt("IDSistema"));
//                s1.setNombre(rs.getString("Nombre"));
//                s1.setLenguaje_desarrollo(rs.getString("Lenguaje_desarrollo"));
//                s1.setEncargado(rs.getInt("Encargado"));
//                s1.setDescripcion(rs.getString("Descripcion"));
//            }
//            
//            conn.CerrarConexion();
//            
//            return s1;
//            
//        }catch(Exception ex)
//        {
//            return null;
//        }
//    }
//    
//    public static void Eliminar(int IDSistema)
//    {
//        try
//        {
//        Conexion conn = new Conexion();
//        
//        String query = "DELETE FROM homeros.Sistema WHERE IDSistema = ?";
//        
//        PreparedStatement ps = conn.Conectar(query);
//        
//        ps.setInt(1, IDSistema);
//        
//        ps.executeUpdate();
//        
//        conn.CerrarConexion();
//        
//        }catch(Exception ex)
//        {
//            
//        }
//    }
//}

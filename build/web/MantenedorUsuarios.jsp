<%-- 
    Document   : MantenedorUsuarios
    Created on : 02-oct-2016, 15:23:27
    Author     : Seba
--%>

<%@page import="Model.UsuarioDTO"%>
<%@page import="Controller.UsuarioDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controller.UsuarioController"%>
<%@page import="Model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/Estilos.css"/>
        <title>Usuarios</title>
        <jsp:include page="index.jsp"></jsp:include>
    </head>
    <body>
        <%
            UsuarioDTO u2 = new UsuarioDTO();
            UsuarioDAO usuarioDAO = new UsuarioDAO();
            
            
            if(request.getParameter("Rut") != null && request.getParameter("Eliminar") == null)
                               {
                u2 = usuarioDAO.seleccionarPorId(Integer.parseInt(request.getParameter("Rut")));
            }else if(request.getParameter("Rut") != null && request.getParameter("Eliminar") != null)
                               {
                UsuarioDAO usuarioDAO2 = new UsuarioDAO();
                usuarioDAO2.eliminar(Integer.parseInt(request.getParameter("Rut")));
            }
            
        %>
        <h1>Mantenedor Usuarios</h1>
        
        
        
        <form action="MantenedorUsuarios.jsp" method="POST">
        <pre>                 
                Rut :            <input type="text" name="txtRut" value="<%=u2.getRut()%>" required>-<input type="text" name="txtDV" value="<%=u2.getDV()%>" id="txtDV" required>

                Nombre :        <input type="text" name="txtNombre" value="<%=u2.getNombre()%>" required>

                Apellidos :     <input type="text" name="txtApellidos" value="<%=u2.getApellidos()%>" required>

                Sexo :          <input type="text" name="txtSexo" value="<%=u2.getSexo()%>" required>

                Edad :          <input type="number" name="txtEdad" value="<%=u2.getEdad()%>" required>

                Dirección :     <input type="text" name="txtDireccion" value="<%=u2.getDireccion()%>" required>

                Correo :        <input type="email" name="txtCorreo" value="<%=u2.getCorreo()%>" required>

                Teléfono :      <input type="text" name="txtTelefono" value="<%=u2.getTelefono()%>" required>

                Rol :           <input type="text" name="txtRol" value="<%=u2.getRol()%>" required>

                Usuario :       <input type="text" name="txtUsername" value="<%=u2.getUsername() %>">
                
                Contraseña :    <input type="password" name="txtPass" value="<%=u2.getPass() %>">

                

                <input type="submit" value="Agregar" name="btnAgregar">          <input type="submit" value="Modificar" name="btnModificar">    
        </pre>
            
            <h2>Usuarios</h2>
             <%
              int errores = 0;
              if(request.getParameter("txtRut") == null || request.getParameter("txtRut").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtDV") == null || request.getParameter("txtDV").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtNombre") == null || request.getParameter("txtNombre").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtSexo") == null || request.getParameter("txtSexo").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtEdad") == null || request.getParameter("txtEdad").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtDireccion") == null || request.getParameter("txtDireccion").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtCorreo") == null || request.getParameter("txtCorreo").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtTelefono") == null || request.getParameter("txtTelefono").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtRol") == null || request.getParameter("txtRol").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtApellidos") == null || request.getParameter("txtApellidos").equals(""))
                                   {
                  errores++;
              }
              if((errores == 0) && (request.getParameter("btnAgregar") != null))
                                   {
                  UsuarioDTO u1 = new UsuarioDTO();
                  u1.setRut(Integer.parseInt(request.getParameter("txtRut")));
                  u1.setDV(request.getParameter("txtDV").trim());
                  u1.setNombre(request.getParameter("txtNombre").trim());
                  u1.setSexo(request.getParameter("txtSexo").trim());
                  u1.setEdad(Integer.parseInt(request.getParameter("txtEdad")));
                  u1.setDireccion(request.getParameter("txtDireccion").trim());
                  u1.setCorreo(request.getParameter("txtCorreo").trim());
                  u1.setTelefono(request.getParameter("txtTelefono").trim());
                  u1.setRol(request.getParameter("txtRol").trim());
                  u1.setApellidos(request.getParameter("txtApellidos").trim());
                  u1.setUsername(request.getParameter("txtUsername").trim());
                  u1.setPass(request.getParameter("txtPass").trim());
                  
                  usuarioDAO.agregar(u1);
              }
              
              if((errores == 0) && (request.getParameter("btnModificar") != null))
              {
                  UsuarioDTO u1 = new UsuarioDTO();
                  u1.setRut(Integer.parseInt(request.getParameter("txtRut")));
                  u1.setDV(request.getParameter("txtDV").trim());
                  u1.setNombre(request.getParameter("txtNombre").trim());
                  u1.setSexo(request.getParameter("txtSexo").trim());
                  u1.setEdad(Integer.parseInt(request.getParameter("txtEdad")));
                  u1.setDireccion(request.getParameter("txtDireccion").trim());
                  u1.setCorreo(request.getParameter("txtCorreo").trim());
                  u1.setTelefono(request.getParameter("txtTelefono").trim());
                  u1.setRol(request.getParameter("txtRol").trim());
                  u1.setApellidos(request.getParameter("txtApellidos").trim());
                  u1.setPass(request.getParameter("txtPass").trim());
                  
                  usuarioDAO.modificar(u1);
                  
              }
              %>
              
              
            <% 
                ArrayList<UsuarioDTO> lista = usuarioDAO.listar();
            %>
            
                
          <table border="1">
              <tr>
                  <th>Rut</th>
                  <th>DV</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Sexo</th>
                  <th>Edad</th>
                  <th>Dirección</th>
                  <th>Correo</th>
                  <th>Teléfono</th>
                  <th>Rol</th>
                  <th>Usuario</th>
                  <th>Contraseña</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
              </tr> 
              
              <%for(UsuarioDTO u1 : lista)
              {
              %>
              <tr>
                  <td><%out.print(u1.getRut());%></td>
                  <td><%out.print(u1.getDV());%></td>
                  <td><%out.print(u1.getNombre());%></td>
                  <td><%out.print(u1.getApellidos());%></td>
                  <td><%out.print(u1.getSexo());%></td>
                  <td><%out.print(u1.getEdad());%></td>
                  <td><%out.print(u1.getDireccion());%></td>
                  <td><%out.print(u1.getCorreo());%></td>
                  <td><%out.print(u1.getTelefono());%></td>
                  <td><%out.print(u1.getRol());%></td>
                  <td><%out.print(u1.getUsername());%></td>
                  <td><%out.print(u1.getPass());%></td>
                  <td><a href="MantenedorUsuarios.jsp?Rut=<%=u1.getRut()%>">Modificar</a></td>
                  <td><a href="MantenedorUsuarios.jsp?Rut=<%=u1.getRut()%>&Eliminar=1">Eliminar</a></td>
              </tr>
              <%
              }
              %>
              
             
              
          </table>
            
        </form>
    </body>
</html>

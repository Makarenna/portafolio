<%-- 
    Document   : MantenedorServicios
    Created on : 03-oct-2016, 16:07:32
    Author     : Seba
--%>

<%@page import="Controller.ServicioDAO"%>
<%@page import="Model.ServicioDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="Conexion.Conexion"%>
<%@page import="Model.Servicio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Servicios</title>
        <link rel="stylesheet" href="css/Estilos.css"/>
        <jsp:include page="index.jsp"></jsp:include>
    </head>
    <body>
        <%
            ServicioDTO se2 = new ServicioDTO();
            ServicioDAO ServicioDAO = new ServicioDAO();
            
            if(request.getParameter("IDServicio") != null && request.getParameter("Eliminar") == null)
                               {
                se2 = ServicioDAO.seleccionarPorId(Integer.parseInt(request.getParameter("IDServicio")));
            }else if(request.getParameter("IDServicio") != null && request.getParameter("Eliminar") != null)
                               {
                ServicioDAO servicioDAO2 = new ServicioDAO();
                servicioDAO2.eliminar(Integer.parseInt(request.getParameter("IDServicio")));
            }        
        %>
        
        <h1>Mantenedor Servicios</h1>
        
        
        
        <form action="MantenedorServicios.jsp" method="POST">
        <pre>                

                Nombre :        <input type="text" name="txtNombre" value="<%=se2.getNombre()%>" required>

                Sistema :       <input type="text" name="txtSistema" value="<%=se2.getSistema()%>" required>

                <input type="hidden" name="hdnIDServicio" value="<%=se2.getIDServicio()%>">

                

                <input type="submit" value="Agregar" name="btnAgregar">          <input type="submit" value="Modificar" name="btnModificar">    
        </pre>
                
                <h2>Servicios</h2>
             <%
              int errores = 0;
              if(request.getParameter("txtNombre") == null || request.getParameter("txtNombre").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtSistema") == null || request.getParameter("txtSistema").equals(""))
                                   {
                  errores++;
              }
              
              if((errores == 0) && (request.getParameter("btnAgregar") != null))
                                   {
                  ServicioDTO se1 = new ServicioDTO();
                  se1.setNombre(request.getParameter("txtNombre"));
                  se1.setSistema(request.getParameter("txtSistema"));
                  ServicioDAO.agregar(se1);
              }
              
              if((errores == 0) && (request.getParameter("btnModificar") != null))
              {
                  ServicioDTO se1 = new ServicioDTO();
                  se1.setNombre(request.getParameter("txtNombre"));
                  se1.setSistema(request.getParameter("txtSistema"));
                  se1.setIDServicio(Integer.parseInt(request.getParameter("hdnIDServicio")));
                  ServicioDAO.modificar(se1);
                  
              }
              %>
              
              <% ArrayList<ServicioDTO> lista = ServicioDAO.listar();%>
            
                
          <table border="1">
              <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Sistema</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
              </tr> 
              
              <%for(ServicioDTO se1 : lista)
              {
              %>
              <tr>
                  <td><%out.print(se1.getIDServicio());%></td>
                  <td><%out.print(se1.getNombre());%></td>
                  <td><%out.print(se1.getSistema());%></td>
                  <td><a href="MantenedorServicios.jsp?IDServicio=<%=se1.getIDServicio()%>">Modificar</a></td>
                  <td><a href="MantenedorServicios.jsp?IDServicio=<%=se1.getIDServicio()%>&Eliminar=1">Eliminar</a></td>
              </tr>
              <%
              }
              %>
              
             
              
          </table>
            
        </form>
    </body>
</html>

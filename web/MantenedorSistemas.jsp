<%-- 
    Document   : MantenedorSistemas
    Created on : 02-oct-2016, 21:30:24
    Author     : Seba
--%>

<%@page import="Controller.SistemaDAO"%>
<%@page import="Model.SistemaDTO"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="Conexion.Conexion"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.Sistema"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistemas</title>
        <link rel="stylesheet" href="css/Estilos.css"/>
        <jsp:include page="index.jsp"></jsp:include>
    </head>
    <body>
        <%
            SistemaDTO s2 = new SistemaDTO();
            SistemaDAO SistemaDAO = new SistemaDAO();
            
            if(request.getParameter("IDSistema") != null && request.getParameter("Eliminar") == null)
                               {
                s2 = SistemaDAO.seleccionarPorId(Integer.parseInt(request.getParameter("IDSistema")));
            }else if(request.getParameter("IDSistema") != null && request.getParameter("Eliminar") != null)
                               {
                SistemaDAO sistemaDAO2 = new SistemaDAO();
                sistemaDAO2.eliminar(Integer.parseInt(request.getParameter("IDSistema")));
            }
            
            Conexion conn = Conexion.getInstance();
            Statement stm = conn.conectar().createStatement();
            String query = "SELECT Rut,Nombre FROM homeros.Usuario";
            ResultSet rs = stm.executeQuery(query);
            
        %>
        <h1>Mantenedor Sistemas</h1>
        
        
        
        <form action="MantenedorSistemas.jsp" method="POST">
        <pre>                

                Nombre :        <input type="text" name="txtNombre" value="<%=s2.getNombre()%>" required>

                Lenguaje :      <input type="text" name="txtLenguaje_desarrollo" value="<%=s2.getLenguaje_desarrollo()%>" required>

                Encargado :     <select name="txtEncargado"><% 
                                    while (rs.next()) 
                                    { 
                                        int fRut = rs.getInt("Rut");
                                        String fnombre = rs.getString("Nombre");
                                    %>
                                    <option value="<%=fRut%>" selected><%=fnombre%></option>
<%
}
%>
                                </select>

                Descripcion :   <textarea name="txtDescripcion" value="<%=s2.getDescripcion()%>"><%=s2.getDescripcion()%></textarea>

                <input type="hidden" name="hdnIDSistema" value="<%=s2.getIDSistema()%>">

                

                <input type="submit" value="Agregar" name="btnAgregar">          <input type="submit" value="Modificar" name="btnModificar">    
        </pre>
            
            <h2>Sistemas</h2>
             <%
              int errores = 0;
              if(request.getParameter("txtNombre") == null || request.getParameter("txtNombre").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtLenguaje_desarrollo") == null || request.getParameter("txtLenguaje_desarrollo").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtEncargado") == null || request.getParameter("txtEncargado").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtDescripcion") == null || request.getParameter("txtDescripcion").equals(""))
                                   {
                  errores++;
              }
              
              if((errores == 0) && (request.getParameter("btnAgregar") != null))
                                   {
                  SistemaDTO s1 = new SistemaDTO();
                  s1.setNombre(request.getParameter("txtNombre").trim());
                  s1.setLenguaje_desarrollo(request.getParameter("txtLenguaje_desarrollo").trim());
                  s1.setEncargado(Integer.parseInt(request.getParameter("txtEncargado")));
                  s1.setDescripcion(request.getParameter("txtDescripcion").trim());
                  SistemaDAO.agregar(s1);
              }
              
              if((errores == 0) && (request.getParameter("btnModificar") != null))
              {
                  SistemaDTO s1 = new SistemaDTO();
                  s1.setNombre(request.getParameter("txtNombre").trim());
                  s1.setLenguaje_desarrollo(request.getParameter("txtLenguaje_desarrollo").trim());
                  s1.setEncargado(Integer.parseInt(request.getParameter("txtEncargado")));
                  s1.setDescripcion(request.getParameter("txtDescripcion").trim());
                  s1.setIDSistema(Integer.parseInt(request.getParameter("hdnIDSistema")));
                  SistemaDAO.modificar(s1);
                  
              }
              %>
              
              
            <% ArrayList<SistemaDTO> lista = SistemaDAO.listar();%>
            
                
          <table border="1">
              <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Lenguaje Desarrollo</th>
                  <th>Encargado</th>
                  <th>Descripción</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
              </tr> 
              
              <%for(SistemaDTO s1 : lista)
              {
              %>
              <tr>
                  <td><%out.print(s1.getIDSistema());%></td>
                  <td><%out.print(s1.getNombre());%></td>
                  <td><%out.print(s1.getLenguaje_desarrollo());%></td>
                  <td><%out.print(s1.getEncargado());%></td>
                  <td><%out.print(s1.getDescripcion());%></td>
                  <td><a href="MantenedorSistemas.jsp?IDSistema=<%=s1.getIDSistema()%>">Modificar</a></td>
                  <td><a href="MantenedorSistemas.jsp?IDSistema=<%=s1.getIDSistema()%>&Eliminar=1">Eliminar</a></td>
              </tr>
              <%
              }
              %>
              
             
              
          </table>
            
        </form>
    </body>
</html>

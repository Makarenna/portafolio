<%-- 
    Document   : MantenedorRacks
    Created on : 03-oct-2016, 15:08:14
    Author     : Seba
--%>

<%@page import="Controller.RackDAO"%>
<%@page import="Model.RackDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="Conexion.Conexion"%>
<%@page import="Model.Rack"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Racks</title>
        <link rel="stylesheet" href="css/Estilos.css"/>
        <jsp:include page="index.jsp"></jsp:include>
    </head>
    <body>
        <%
            RackDTO r2 = new RackDTO();
            RackDAO RackDAO = new RackDAO();
            
            if(request.getParameter("IDRack") != null && request.getParameter("Eliminar") == null)
                               {
                r2 = RackDAO.seleccionarPorId(Integer.parseInt(request.getParameter("IDRack")));
            }else if(request.getParameter("IDRack") != null && request.getParameter("Eliminar") != null)
                               {
                RackDAO rackDAO2 = new RackDAO();
                rackDAO2.eliminar(Integer.parseInt(request.getParameter("IDRack")));
            }
            
            Conexion conn = Conexion.getInstance();
            Statement stm = conn.conectar().createStatement();
            String query = "SELECT IDSala,Nombre FROM homeros.Sala_servidor";
            ResultSet rs = stm.executeQuery(query);            
        %>
        
        <h1>Mantenedor Racks</h1>
        
        
        
        <form action="MantenedorRacks.jsp" method="POST">
        <pre>                

                Numero :        <input type="number" name="txtNumero" value="<%=r2.getNumero()%>" required>

                Capacidad :         <input type="number" name="txtCapacidad" value="<%=r2.getCapacidad()%>" required> Kg.

          Sala de Servidores :  <select name="txtSala_servidor"><% 
                                    while (rs.next()) 
                                    { 
                                        int fIDSala = rs.getInt("IDSala");
                                        String fnombre = rs.getString("Nombre");
                                    %>
                                    <option value="<%=fIDSala%>"><%=fnombre%></option>
<%
}
%>
                                </select>

                <input type="hidden" name="hdnIDRack" value="<%=r2.getIDRack()%>">

                

                <input type="submit" value="Agregar" name="btnAgregar">          <input type="submit" value="Modificar" name="btnModificar">    
        </pre>
                
                <h2>Racks</h2>
             <%
              int errores = 0;
              if(request.getParameter("txtNumero") == null || request.getParameter("txtNumero").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtCapacidad") == null || request.getParameter("txtCapacidad").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtSala_servidor") == null || request.getParameter("txtSala_servidor").equals(""))
                                   {
                  errores++;
              }
              
              if((errores == 0) && (request.getParameter("btnAgregar") != null))
                                   {
                  RackDTO r1 = new RackDTO();
                  r1.setNumero(Integer.parseInt(request.getParameter("txtNumero")));
                  r1.setCapacidad(Integer.parseInt(request.getParameter("txtCapacidad")));
                  r1.setSala_servidor(Integer.parseInt(request.getParameter("txtSala_servidor")));
                  RackDAO.agregar(r1);
              }
              
              if((errores == 0) && (request.getParameter("btnModificar") != null))
              {
                  RackDTO r1 = new RackDTO();
                  r1.setNumero(Integer.parseInt(request.getParameter("txtNumero")));
                  r1.setCapacidad(Integer.parseInt(request.getParameter("txtCapacidad")));
                  r1.setSala_servidor(Integer.parseInt(request.getParameter("txtSala_servidor")));
                  r1.setIDRack(Integer.parseInt(request.getParameter("hdnIDRack")));
                  RackDAO.modificar(r1);
                  
              }
              %>
              
              <% ArrayList<RackDTO> lista = RackDAO.listar();%>
            
                
          <table border="1">
              <tr>
                  <th>#</th>
                  <th>Numero</th>
                  <th>Capacidad Kg.</th>
                  <th>Sala de Servidores</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
              </tr> 
              
              <%for(RackDTO r1 : lista)
              {
              %>
              <tr>
                  <td><%out.print(r1.getIDRack());%></td>
                  <td><%out.print(r1.getNumero());%></td>
                  <td><%out.print(r1.getCapacidad());%></td>
                  <td><%out.print(r1.getSala_servidor());%></td>
                  <td><a href="MantenedorRacks.jsp?IDRack=<%=r1.getIDRack()%>">Modificar</a></td>
                  <td><a href="MantenedorRacks.jsp?IDRack=<%=r1.getIDRack()%>&Eliminar=1">Eliminar</a></td>
              </tr>
              <%
              }
              %>
              
             
              
          </table>
            
        </form>
    </body>
</html>

<%-- 
    Document   : MantenedorSala_servidores
    Created on : 03-oct-2016, 1:54:39
    Author     : Seba
--%>

<%@page import="Controller.Sala_ServidorDAO"%>
<%@page import="Model.Sala_ServidorDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="Conexion.Conexion"%>
<%@page import="Model.Sala_Servidor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Salas de Servidores</title>
        <link rel="stylesheet" href="css/Estilos.css"/>
        <jsp:include page="index.jsp"></jsp:include>
    </head>
    <body>
        <%
            Sala_ServidorDTO ss2 = new Sala_ServidorDTO();
            Sala_ServidorDAO Sala_ServidorDAO = new Sala_ServidorDAO();
            
            if(request.getParameter("IDSala") != null && request.getParameter("Eliminar") == null)
                               {
                ss2 = Sala_ServidorDAO.seleccionarPorId(Integer.parseInt(request.getParameter("IDSala")));
            }else if(request.getParameter("IDSala") != null && request.getParameter("Eliminar") != null)
                               {
                Sala_ServidorDAO sala_ServidorDAO2 = new Sala_ServidorDAO();
                sala_ServidorDAO2.eliminar(Integer.parseInt(request.getParameter("IDSala")));
            }
            
            Conexion conn = Conexion.getInstance();
            Statement stm = conn.conectar().createStatement();
            String query = "SELECT Rut,Nombre FROM homeros.Usuario";
            ResultSet rs = stm.executeQuery(query);
            
        %>
        <h1>Mantenedor Salas</h1>
        
        
        
        <form action="MantenedorSala_servidores.jsp" method="POST">
        <pre>                

                Nombre :        <input type="text" name="txtNombre" value="<%=ss2.getNombre()%>" required>

                País :          <input type="text" name="txtPais" value="<%=ss2.getPais()%>" required>

                Region :        <input type="text" name="txtRegion" value="<%=ss2.getRegion()%>" required>

                Comuna :        <input type="text" name="txtComuna" value="<%=ss2.getComuna()%>" required>

                Calle :         <input type="text" name="txtCalle" value="<%=ss2.getCalle()%>" required>

                Telefono :      <input type="text" name="txtTel_contacto" value="<%=ss2.getTel_contacto()%>" required>

                Encargado :     <select name="txtEncargado"><% 
                                    while (rs.next()) 
                                    { 
                                        int fRut = rs.getInt("Rut");
                                        String fnombre = rs.getString("Nombre");
                                    %>
                                    <option value="<%=fRut%>"><%=fnombre%></option>
<%
}
%>
                                </select>

                <input type="hidden" name="hdnIDSala" value="<%=ss2.getIDSala()%>">

                

                <input type="submit" value="Agregar" name="btnAgregar">          <input type="submit" value="Modificar" name="btnModificar">    
        </pre>
                
        <h2>Salas</h2>
             <%
              int errores = 0;
              if(request.getParameter("txtNombre") == null || request.getParameter("txtNombre").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtPais") == null || request.getParameter("txtPais").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtRegion") == null || request.getParameter("txtRegion").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtComuna") == null || request.getParameter("txtComuna").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtCalle") == null || request.getParameter("txtCalle").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtTel_contacto") == null || request.getParameter("txtTel_contacto").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtEncargado") == null || request.getParameter("txtEncargado").equals(""))
                                   {
                  errores++;
              }
              
              if((errores == 0) && (request.getParameter("btnAgregar") != null))
                                   {
                  Sala_ServidorDTO ss1 = new Sala_ServidorDTO();
                  ss1.setNombre(request.getParameter("txtNombre").trim());
                  ss1.setPais(request.getParameter("txtPais").trim());
                  ss1.setRegion(request.getParameter("txtRegion").trim());
                  ss1.setComuna(request.getParameter("txtComuna").trim());
                  ss1.setCalle(request.getParameter("txtCalle").trim());
                  ss1.setTel_contacto(request.getParameter("txtTel_contacto").trim());
                  ss1.setEncargado(Integer.parseInt(request.getParameter("txtEncargado")));
                  Sala_ServidorDAO.agregar(ss1);
              }
              
              if((errores == 0) && (request.getParameter("btnModificar") != null))
              {
                  Sala_ServidorDTO ss1 = new Sala_ServidorDTO();
                  ss1.setNombre(request.getParameter("txtNombre").trim());
                  ss1.setPais(request.getParameter("txtPais").trim());
                  ss1.setRegion(request.getParameter("txtRegion").trim());
                  ss1.setComuna(request.getParameter("txtComuna").trim());
                  ss1.setCalle(request.getParameter("txtCalle").trim());
                  ss1.setTel_contacto(request.getParameter("txtTel_contacto").trim());
                  ss1.setEncargado(Integer.parseInt(request.getParameter("txtEncargado")));
                  ss1.setIDSala(Integer.parseInt(request.getParameter("hdnIDSala")));
                  Sala_ServidorDAO.modificar(ss1);
                  
              }
              %>
              
              <% ArrayList<Sala_ServidorDTO> lista = Sala_ServidorDAO.listar();%>
            
                
          <table border="1">
              <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>País</th>
                  <th>Region</th>
                  <th>Comuna</th>
                  <th>Calle</th>
                  <th>Telefono de Contacto</th>
                  <th>Encargado</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
              </tr> 
              
              <%for(Sala_ServidorDTO ss1 : lista)
              {
              %>
              <tr>
                  <td><%out.print(ss1.getIDSala());%></td>
                  <td><%out.print(ss1.getNombre());%></td>
                  <td><%out.print(ss1.getPais());%></td>
                  <td><%out.print(ss1.getRegion());%></td>
                  <td><%out.print(ss1.getComuna());%></td>
                  <td><%out.print(ss1.getCalle());%></td>
                  <td><%out.print(ss1.getTel_contacto());%></td>
                  <td><%out.print(ss1.getEncargado());%></td>
                  <td><a href="MantenedorSala_servidores.jsp?IDSala=<%=ss1.getIDSala()%>">Modificar</a></td>
                  <td><a href="MantenedorSala_servidores.jsp?IDSala=<%=ss1.getIDSala()%>&Eliminar=1">Eliminar</a></td>
              </tr>
              <%
              }
              %>
              
             
              
          </table>
            
        </form>
    </body>
</html>

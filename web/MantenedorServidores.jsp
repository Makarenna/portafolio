<%-- 
    Document   : MantenedorServidores
    Created on : 03-oct-2016, 17:21:37
    Author     : Seba
--%>

<%@page import="Controller.ServidorDAO"%>
<%@page import="Model.ServidorDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="Conexion.Conexion"%>
<%@page import="Model.Servidor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Servidores</title>
        <link rel="stylesheet" href="css/Estilos.css"/>
        <jsp:include page="index.jsp"></jsp:include>
    </head>
    <body>
        <%
            ServidorDTO sr2 = new ServidorDTO();
            ServidorDAO ServidorDAO = new ServidorDAO();
            
            if(request.getParameter("IDServidor") != null && request.getParameter("Eliminar") == null)
                               {
                sr2 = ServidorDAO.seleccionarPorId(Integer.parseInt(request.getParameter("IDServidor")));
            }else if(request.getParameter("IDServidor") != null && request.getParameter("Eliminar") != null)
                               {
                ServidorDAO servidorDAO2 = new ServidorDAO();
                servidorDAO2.eliminar(Integer.parseInt(request.getParameter("IDServidor")));
            }
            
            Conexion conn = Conexion.getInstance();
            Statement stm = conn.conectar().createStatement();
            String query = "SELECT IDSistema,Nombre FROM homeros.Sistema";
            ResultSet rs = stm.executeQuery(query);
            
        %>
        
        <h1>Mantenedor Servidores</h1>
        
        
        
        <form action="MantenedorServidores.jsp" method="POST">
        <pre>                

                Nombre :        <input type="text" name="txtNombre" value="<%=sr2.getNombre()%>" required>

                Sistema :       <select name="txtSistema"><% 
                                    while (rs.next()) 
                                    { 
                                        int fIDSistema = rs.getInt("IDSistema");
                                        String fNombreSistema = rs.getString("Nombre");
                                    %>
                                    <option value="<%=fIDSistema%>"><%=fNombreSistema%></option>
<%
}
%>
                                </select>

                Procesador :    <input type="text" name="txtProcesador" value="<%=sr2.getProcesador()%>" required>

                Disco Duro :    <input type="text" name="txtDisco_duro" value="<%=sr2.getDisco_duro()%>" required>

                Memoria Ram :   <input type="text" name="txtMem_ram" value="<%=sr2.getMem_ram()%>" required>

                Direccion IP :  <input type="text" name="txtDirec_ip" value="<%=sr2.getDirec_ip()%>" required>

                Rack :          <select name="txtRack"><%
                    Statement stm1 = conn.conectar().createStatement();
            String query1 = "SELECT IDRack,Numero FROM homeros.Rack";
            ResultSet rs1 = stm1.executeQuery(query1);
                                    while (rs1.next()) 
                                    { 
                                        int fIDRack = rs1.getInt("IDRack");
                                        int fNumeroRack = rs1.getInt("Numero");
                                    %>
                                    <option value="<%=fIDRack%>"><%=fNumeroRack%></option>
<%
}
%>
                                </select>

                Garantia :      <select name="txtGarantia"><%
                    Statement stm2 = conn.conectar().createStatement();
            String query2 = "SELECT IDGarantia,Nombre FROM homeros.Garantia";
            ResultSet rs2 = stm2.executeQuery(query2);
                                    while (rs2.next()) 
                                    { 
                                        int fIDG = rs2.getInt("IDGarantia");
                                        String fNombreG = rs2.getString("Nombre");
                                    %>
                                    <option value="1">Sin Garantia</option>
                                    <option disabled>------------Con Garantia-----------</option>
                                    <option value="<%=fIDG%>"><%=fNombreG%></option>
<%
}
%>
                                </select>

                Tipo :          <select name="txtTipo"><%
                    Statement stm3 = conn.conectar().createStatement();
            String query3 = "SELECT IDServidor_BD,Usuario FROM homeros.Servidor_BD";
            ResultSet rs3 = stm3.executeQuery(query3);
                                    while (rs3.next()) 
                                    { 
                                        int fIDSE = rs3.getInt("IDServidor_BD");
                                        String fUsuarioSE = rs3.getString("Usuario");
                                    %>
                                    <option value="1">Aplicaciones</option>
                                    <option disabled>------------Base de Datos-----------</option>
                                    <option value="<%=fIDSE%>"><%=fUsuarioSE%></option>
<%
}
%>
                                </select>

                Contraseña :    <input type="text" name="txtContrasenia" value="<%=sr2.getContrasenia()%>" required>

                Encargado :     <select name="txtEncargado"><%
                    Statement stm4 = conn.conectar().createStatement();
            String query4 = "SELECT Rut,Nombre FROM homeros.Usuario";
            ResultSet rs4 = stm4.executeQuery(query4);
                                    while (rs4.next()) 
                                    { 
                                        int fRut = rs4.getInt("Rut");
                                        String fNombreU = rs4.getString("Nombre");
                                    %>
                                    <option value="<%=fRut%>"><%=fNombreU%></option>
<%
}
%>
                                </select>


                <input type="hidden" name="hdnIDServidor" value="<%=sr2.getIDServidor()%>">

                

                <input type="submit" value="Agregar" name="btnAgregar">          <input type="submit" value="Modificar" name="btnModificar">    
        </pre>
                
        <h2>Servidores</h2>
             <%
              int errores = 0;
              if(request.getParameter("txtNombre") == null || request.getParameter("txtNombre").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtSistema") == null || request.getParameter("txtSistema").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtProcesador") == null || request.getParameter("txtProcesador").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtDisco_duro") == null || request.getParameter("txtDisco_duro").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtMem_ram") == null || request.getParameter("txtMem_ram").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtDirec_ip") == null || request.getParameter("txtDirec_ip").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtRack") == null || request.getParameter("txtRack").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtGarantia") == null || request.getParameter("txtGarantia").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtTipo") == null || request.getParameter("txtTipo").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtContrasenia") == null || request.getParameter("txtContrasenia").equals(""))
                                   {
                  errores++;
              }
              if(request.getParameter("txtEncargado") == null || request.getParameter("txtEncargado").equals(""))
                                   {
                  errores++;
              }
              
              if((errores == 0) && (request.getParameter("btnAgregar") != null))
                                   {
                  ServidorDTO sr1 = new ServidorDTO();
                  sr1.setNombre(request.getParameter("txtNombre").trim());
                  sr1.setSis_operativo(Integer.parseInt(request.getParameter("txtSistema")));
                  sr1.setProcesador(request.getParameter("txtProcesador").trim());
                  sr1.setDisco_duro(request.getParameter("txtDisco_duro").trim());
                  sr1.setMem_ram(request.getParameter("txtMem_ram").trim());
                  sr1.setDirec_ip(request.getParameter("txtDirec_ip").trim());
                  sr1.setRack(Integer.parseInt(request.getParameter("txtRack")));
                  sr1.setGarantia(Integer.parseInt(request.getParameter("txtGarantia")));
                  sr1.setTipo(Integer.parseInt(request.getParameter("txtTipo")));
                  sr1.setContrasenia(request.getParameter("txtContrasenia").trim());
                  sr1.setEncargado(Integer.parseInt(request.getParameter("txtEncargado")));
                  ServidorDAO.agregar(sr1);
              }
              
              if((errores == 0) && (request.getParameter("btnModificar") != null))
              {
                  ServidorDTO sr1 = new ServidorDTO();
                  sr1.setNombre(request.getParameter("txtNombre").trim());
                  sr1.setSis_operativo(Integer.parseInt(request.getParameter("txtSistema")));
                  sr1.setProcesador(request.getParameter("txtProcesador").trim());
                  sr1.setDisco_duro(request.getParameter("txtDisco_duro").trim());
                  sr1.setMem_ram(request.getParameter("txtMem_ram").trim());
                  sr1.setDirec_ip(request.getParameter("txtDirec_ip").trim());
                  sr1.setRack(Integer.parseInt(request.getParameter("txtRack")));
                  sr1.setGarantia(Integer.parseInt(request.getParameter("txtGarantia")));
                  sr1.setTipo(Integer.parseInt(request.getParameter("txtTipo")));
                  sr1.setContrasenia(request.getParameter("txtContrasenia").trim());
                  sr1.setEncargado(Integer.parseInt(request.getParameter("txtEncargado")));
                  sr1.setIDServidor(Integer.parseInt(request.getParameter("hdnIDServidor")));
                  ServidorDAO.modificar(sr1);
                  
              }
              %>
              
              <% ArrayList<ServidorDTO> lista = ServidorDAO.listar();%>
            
                
          <table border="1">
              <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Sistema</th>
                  <th>Procesador</th>
                  <th>Disco Duro</th>
                  <th>Memoria Ram</th>
                  <th>Dirección IP</th>
                  <th>Rack</th>
                  <th>Garantia</th>
                  <th>Tipo</th>
                  <th>Contraseña</th>
                  <th>Encargado</th>
                  <th>Modificar</th>
                  <th>Eliminar</th>
              </tr> 
              
              <%for(ServidorDTO sr1 : lista)
              {
              %>
              <tr>
                  <td><%out.print(sr1.getIDServidor());%></td>
                  <td><%out.print(sr1.getNombre());%></td>
                  <td><%out.print(sr1.getSis_operativo());%></td>
                  <td><%out.print(sr1.getProcesador());%></td>
                  <td><%out.print(sr1.getDisco_duro());%></td>
                  <td><%out.print(sr1.getMem_ram());%></td>
                  <td><%out.print(sr1.getDirec_ip());%></td>
                  <td><%out.print(sr1.getRack());%></td>
                  <td><%out.print(sr1.getGarantia());%></td>
                  <td><%out.print(sr1.getTipo());%></td>
                  <td><%out.print(sr1.getContrasenia());%></td>
                  <td><%out.print(sr1.getEncargado());%></td>
                  <td><a href="MantenedorServidores.jsp?IDServidor=<%=sr1.getIDServidor()%>">Modificar</a></td>
                  <td><a href="MantenedorServidores.jsp?IDServidor=<%=sr1.getIDServidor()%>&Eliminar=1">Eliminar</a></td>
              </tr>
              <%
              }
              %>
              
             
              
          </table>
            
        </form>
    </body>
</html>

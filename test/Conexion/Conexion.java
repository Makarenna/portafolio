/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author Seba
 */
public class Conexion {
    private Connection conn = null;
    private String JDBC_DRIVER = "oracle.jdbc.OracleDriver";
    private String username = "system";
    private String password = "seba";
    private String DataBaseName = "jdbc:oracle:thin:@localhost:1521:XE";
    
    public Statement Conectar()
    {
     try
     {
       Class.forName(JDBC_DRIVER);
       conn = DriverManager.getConnection(DataBaseName,username,password);
       return conn.createStatement();
       
     }catch(Exception ex)
     {
         return null;
     }
    }
    
    public PreparedStatement Conectar(String query)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DataBaseName,username,password);
            return conn.prepareStatement(query);
        }catch(Exception ex)
        {
            return null;
        }
    }
    
    public void CerrarConexion()
    {
        try
        {
            conn.close();
        }catch(Exception e)
        {
            
        }
    }
}
